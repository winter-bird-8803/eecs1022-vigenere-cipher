package ca.yorku.eecs.kryptonote;



import org.junit.Assert;
import org.junit.Test;

public class CipherTest
{
    @Test
    public void testCipher() {
        Cipher testCipher;

        testCipher = new Cipher("1");
        Assert.assertEquals("A1", "BBB", testCipher.Encrypt("AAA"));
        Assert.assertEquals("A2", "AAA", testCipher.Decrypt("BBB"));

        testCipher = new Cipher("1234");
        Assert.assertEquals("B1", "UJLWAKVDBBWITV", testCipher.Encrypt("THIS IS A TEST"));
        Assert.assertEquals("B2", "THIS IS A TEST", testCipher.Decrypt("UJLWAKVDBBWITV"));
    }
}
